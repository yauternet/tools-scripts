#!/bin/bash

for cron_file in $(find * -type f -executable) ;do
	ln -s "$(pwd)/$cron_file" /etc/cron.hourly/"$cron_file";
done
